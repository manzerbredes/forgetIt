find_package(PkgConfig)

pkg_check_modules(PC_GTK3 QUIET gtk+-3.0)

set(GTK3_DEFINITIONS ${PC_GTK3_CFLAGS_OTHER})


find_path(GTK3_INCLUDE_DIR
NAMES gtk/gtk.h
PATHS ${PC_GTK3_INCLUDE_DIRS}
)

##message(${PC_GTK3_INCLUDE_DIRS})  

        
find_library(GTK3_LIBRARY NAMES gtk-3
             HINTS ${PC_GTK3_LIBDIR} ${PC_GTK3_LIBRARY_DIRS} )

set(GTK3_LIBRARIES ${GTK3_LIBRARY} )
set(GTK3_INCLUDE_DIRS ${GTK3_INCLUDE_DIR} ${PC_GTK3_INCLUDE_DIRS} )

find_package_handle_standard_args(GTK3  DEFAULT_MSG
                                  GTK3_LIBRARY GTK3_INCLUDE_DIR)

##mark_as_advanced(GTK3_INCLUDE_DIR GTK3_LIBRARY )
