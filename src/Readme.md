Build System
=====

##CMake (v3.0.2)

<br />

Library Versions
=====

##crypto++ (v5.6.1)
##libxml++ (v2.6-2)
##GTK+ (v3.14)

<br />

Class organisation
=====

##CryptClass
> Contain all crypting algorithm, and use
**crypto++** library.

##IOFileClass
> Contain all class that manage IO in file.
*FileManIOFile* class load and save encrypted files, and use
class from *CryptClass*. 

##ParserClass
> Contain class for parsing data (exemple: parsing a decrypted file from *FileManIOFile*.

##GTK+
> Contain the graphical interface, using *gtk+* library.

##Coming soon:
>GTK+Class to manager GUI. 
<br /><br />

PLEASE USE DOXYGEN COMMENTS
=====