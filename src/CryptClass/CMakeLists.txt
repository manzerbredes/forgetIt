#Retrieve crypto++ libraries
get_property(CRYPTO++_LIBRARIES GLOBAL PROPERTY CRYPTO++_LIBRARIES)

#Make CryptClass lib
add_library(CryptClass ./AESCrypt.cpp ./HASHCrypt.cpp) 

#Add crypto++ to CryptClass
target_link_libraries(CryptClass ${CRYPTO++_LIBRARIES})
