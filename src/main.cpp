/**
 * @file main.cpp
 * @brief Entry point
 * @author manzerbredes
 * @version Prototype
 * @date 8 Mars 2015
 *
 * Entry point of the application.
 *
 */




//----- std -----

#include <iostream>
#include <string>
#include <vector>


//----- class -----
#include "./IOFileClass/FileManIOFile.hpp"
#include "./ParserClass/FileManParser.hpp"
#include "./ParserClass/FileManContainer/Website.hpp"


#include <gtk/gtk.h>


/**
 * @fn int main(int argc, char *argv[])
 * @author manzerbredes
 * @brief main function
 * @param argc contain *argv[] length
 * @param *argv[] contain the arguments list
 * @return Return code, an int.
 */
int main(int argc, char *argv[]){



    /* Initialisation de GTK+ */
    gtk_init(&argc, &argv);


    GtkWidget* MainWindow=NULL;

    MainWindow=gtk_window_new(GTK_WINDOW_TOPLEVEL);

    g_signal_connect(G_OBJECT(MainWindow), "delete-event", G_CALLBACK(gtk_main_quit), NULL);


    GtkWidget* bouton;

    bouton=gtk_button_new_with_label("Hello !");
    gtk_container_add(GTK_CONTAINER(MainWindow), bouton);

    g_signal_connect(G_OBJECT(bouton), "leave", G_CALLBACK(gtk_main_quit), NULL);

    gtk_window_set_title(GTK_WINDOW(MainWindow), "forgetIt");
    gtk_window_set_default_size(GTK_WINDOW(MainWindow), 500,500);

    gtk_window_set_position(GTK_WINDOW(MainWindow), GTK_WIN_POS_CENTER);



    gtk_widget_show_all(MainWindow);


    gtk_main();



    return EXIT_SUCCESS;

}



